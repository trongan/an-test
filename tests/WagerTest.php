<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/9/2020
 * Time: 12:16 AM
 */

class WagerTest extends TestCase{

    /**
     * @dataProvider providerTestCreating
     */
    public function testCreatingSuccessFully($inputs){
        $this->json('POST', '/wagers', $inputs)
            ->shouldReturnJson()
            ->seeJson(
                $inputs
            )
            ->assertResponseStatus(201);
        $result = json_decode($this->response->content(), true);
        if(empty($result['error'])){
            $testResult = $inputs;
            $testResult['id'] = $result['id'];
            $testResult['current_selling_price'] = $result['selling_price'];
            $this->seeInDatabase('wagers', array_merge(['id' => $result['id']], $inputs));
        }
    }



    /**
     * @return array
     */
    public function providerTestCreating(){
        return [
            [
                [
                    'total_wager_value'=> 2000,
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 300
                ],

            ],
            [
                [
                    'total_wager_value'=> 150,
                    'odds'=> 8,
                    'selling_percentage'=> 10,
                    'selling_price'=> 25
                ]
            ],
            [
                [
                    'total_wager_value'=> 1000,
                    'odds'=> 5,
                    'selling_percentage'=> 10,
                    'selling_price'=> 300
                ]
            ],
            [
                [
                    'total_wager_value'=> 150,
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 200
                ]
            ]
        ];
    }


    /**
     * @dataProvider providerTestInvalidInputs
     */
    public function testCreatingInvalidInputs($inputs){
        $this->json('POST', '/wagers', $inputs)
            ->seeJsonStructure([
                'error'
            ])
            ->assertResponseStatus(400);
    }


    /**
     * @return array
     */
    public function providerTestInvalidInputs(){
        return [
            [
                [
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 300
                ],

            ],
            [
                [
                    'total_wager_value'=> 1000,
                    'odds'=> 10,
                    'selling_percentage'=> 10
                ]
            ],
            [
                [
                    'total_wager_value'=> 'str',
                    'odds'=> 10,
                    'selling_percentage'=> 10
                ]
            ],
            [
                [
                    'total_wager_value'=> 100,
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 5
                ]
            ],
            [
                [
                    'total_wager_value'=> 100.2,
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 50
                ]
            ],
            [
                [
                    'total_wager_value'=> -100,
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> 50
                ]
            ],
            [
                [
                    'total_wager_value'=> 100,
                    'odds'=> 10,
                    'selling_percentage'=> 10,
                    'selling_price'=> -50
                ]
            ]
        ];
    }
}