<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/9/2020
 * Time: 11:27 AM
 */

class BuyingTest extends TestCase{

    /**
     * @dataProvider dataProviderTestCreating
     *
     * @param $inputs
     * @return bool
     */
    public function testCreatingSuccessFully($inputs){
        $wager = factory(\App\Models\Wager::class)->create([
            'total_wager_value'=> 2000,
            'odds'=> 10,
            'selling_percentage'=> 10,
            'selling_price'=> 300,
            'current_selling_price' =>300
        ]);
        $this->json('POST', '/buy/'.$wager->id, $inputs)
            ->shouldReturnJson()
            ->seeJson([
                'wager_id'=> $wager->id,
                'buying_price'=> $inputs['buying_price']
            ])
            ->assertResponseStatus(201);
        $result = json_decode($this->response->content(), true);
        if(empty($result['error'])){
            $testResult['id'] = $wager->id;
            $testResult['current_selling_price'] = $wager->current_selling_price - $inputs['buying_price'];
            $testResult['amount_sold'] = $wager->amount_sold ? $wager->amount_sold + $inputs['buying_price'] : $inputs['buying_price'];
            $testResult['percentage_sold'] = round(($testResult['amount_sold'] / $wager->selling_price * 100), 2);
            $this->seeInDatabase('wagers', $testResult);
        }
    }

    /**
     * @return array
     */
    public function dataProviderTestCreating(){
        return [
            [
               ['buying_price'=>10]
            ],
            [
                ['buying_price'=>20]
            ],
            [
                ['buying_price'=>10.25]
            ],
            [
                ['buying_price'=>5]
            ],
        ];
    }


    /**
     * @dataProvider dataProviderTestInvalidInputs
     */
    public function testInvalidInputs($inputs){
        $wager = factory(\App\Models\Wager::class)->create([
            'total_wager_value'=> 2000,
            'odds'=> 10,
            'selling_percentage'=> 10,
            'selling_price'=> 300,
            'current_selling_price' =>10
        ]);
        $this->json('POST', '/buy/'.$wager->id, $inputs)
            ->shouldReturnJson()
            ->seeJsonStructure([
                'error'
            ])
            ->assertResponseStatus(400);
    }


    /**
     * @return array
     */
    public function dataProviderTestInvalidInputs(){
        return [
            [
                ['buying_price'=>100]
            ],
            [
                ['buying_price'=>-20]
            ],
            [
                ['buying_price'=>100.25]
            ],
            [
                ['buying_price'=>80]
            ]
        ];
    }
}