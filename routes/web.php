<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([],function() use ($router) {
    /**
     * Creating wager route
     */
    $router->post('/wagers', 'WagerController@create');


     /**
     * Getting list wager route
     */
    $router->get('/wagers', 'WagerController@getList');


    /**
     * Buying route
     */
    $router->post('/buy/{wager_id:[0-9]+}', 'TransactionController@buy');


});


