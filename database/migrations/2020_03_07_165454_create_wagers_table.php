<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wagers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('total_wager_value');
            $table->unsignedInteger('odds');
            $table->unsignedSmallInteger('selling_percentage');
            $table->unsignedDecimal('selling_price', 11, 2);
            $table->unsignedDecimal('current_selling_price', 11, 2);
            $table->float('percentage_sold')->nullable();
            $table->unsignedDecimal('amount_sold', 11, 2)->nullable();
            $table->timestamp('placed_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wagers');
    }
}
