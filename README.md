# Backend Challenge Solution

## Description 
The solution is based on Lumen framework and built with the Docker images: `nginx`, `php 7.2`  and `mysql 5.7`.
The below ports are used when running:

        - nginx: 8080
        - mysql: 3306

## Setup
1. Make sure already installed Docker and Docker compose. 
2. Clone source code from `https://bitbucket.org/trongan/an-test.git`
3. Go to the source code and run `start.sh` bash script to setup all relevant services/applications and database.
4. After running `start.sh` successfully, the server is ready at http://localhost:8080 for testing.

## Response description
1. With success case, the server will return as the API interface descriptions.
2. With the validation fails case, the server will return with http code 400 and body with proper invalid errors. Example when creating wager with invalid inputs:

         {
            "total_wager_value": 0,
            "odds": 10,
            "selling_percentage": 10,
            "selling_price": -10,
         }

    Response:
    
        {
            "error": {
                "total_wager_value": [
                    "The total wager value must be greater than 0."
                ],
                "selling_price": [
                    "The selling price must be greater than 0."
                ]
            }
        }
    
3. With the failed cases, the server will return with http code 500 and proper error code:
      - Creating wager fails: `ERR_CREATING_WAGER_FAILED`
      - Create buying fails: `ERR_CREATING_WAGER_FAILED`
      - General error: `ERR_SOMETHING_WENT_WRONG`
4. With the invalid request, the server will return with http code 404.

## Unit test
The solution also includes unit test. Run `docker-compose exec php php ./vendor/bin/phpunit` to start.


## Author
[trongan@outlook.com](mailto:trongan@outlook.com)