<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 12:21 PM
 */

namespace App\Repositories;


use App\Models\Transaction;
use App\Models\Wager;

class TransactionRepository{


    /**
     * @param $wager Wager
     * @param $inputs
     * @return bool | Transaction
     */
    public function store($wager, $inputs){
        try{
            \DB::beginTransaction();
            $model = new Transaction();
            $model->buying_price = $inputs['buying_price'];
            $model->wager_id = $wager->id;
            $model->save();

            /**
             * Update the wager fields
             */
            $wager->current_selling_price = $wager->current_selling_price - $inputs['buying_price'];
            $wager->amount_sold = $wager->amount_sold ? $wager->amount_sold + $inputs['buying_price'] : $inputs['buying_price'];
            $wager->percentage_sold = round(($wager->amount_sold / $wager->selling_price) * 100, 2);
            $wager->save();
            \DB::commit();
            return $model;
        }
        catch (\Exception $ex){
            report($ex);
            \DB::rollback();
            return false;
        }
    }
}