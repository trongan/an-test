<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 11:47 AM
 */

namespace App\Repositories;


use App\Models\Wager;

class WagerRepository{

    /**
     * Store the Wager model
     *
     * @param $inputs
     * @return Wager|bool
     */
    public function store($inputs){
        try{
            $model = new Wager();
            $model->total_wager_value = $inputs['total_wager_value'];
            $model->odds = $inputs['odds'];
            $model->selling_percentage = $inputs['selling_percentage'];
            $model->selling_price = $inputs['selling_price'];
            $model->current_selling_price  = $inputs['selling_price'];
            $model->percentage_sold = null;
            $model->amount_sold = null;
            $model->save();
            return $model;
        }
        catch (\Exception $ex){
            report($ex);
            return false;
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id){
        return Wager::find($id);
    }


    public function getList($page, $limit){
        $conditions = [];
        $offset = ($page - 1) * $limit;
        $results = Wager::where($conditions)
        ->limit($limit)
        ->offset($offset)
        ->get();
        return $results;
    }
}