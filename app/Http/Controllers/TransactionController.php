<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 12:21 PM
 */

namespace App\Http\Controllers;


namespace App\Http\Controllers;


use App\Http\Validators\MustValidateTransaction;
use App\Repositories\TransactionRepository;
use App\Repositories\WagerRepository;
use Illuminate\Http\Request;


class TransactionController extends Controller{

    use MustValidateTransaction;


    /**
     * Handle buying request
     *
     * @param $wager_id
     * @param Request $request
     * @param TransactionRepository $repository
     * @param WagerRepository $wagerRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function buy($wager_id, Request $request, TransactionRepository $repository, WagerRepository $wagerRepository){
        $inputs = $request->json()->all();
        $wager = $wagerRepository->find($wager_id);
        if(empty($wager)){
            return $this->response([
                'error'=> ERR_WAGER_ID_NOT_FOUND
            ],400);
        }
        $validator = $this->validateBuying($wager, $inputs);
        if($validator->fails()){
            return $this->response([
                'error'=> $validator->errors()
            ],400);
        }
        if($result = $repository->store($wager, $inputs)){
            return $this->response($result, 201);
        }
        else{
            return $this->response([
                'error'=> ERR_CREATED_BUYING_FAILED
            ],500);
        }
    }
}