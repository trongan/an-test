<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController{

    /**
     * @param $data
     * @param int $status
     * @param array $headers
     * @param int $options
     * @return \Illuminate\Http\JsonResponse
     */
    protected function response($data, $status=200, array $headers = [], $options = JSON_NUMERIC_CHECK){
       return response()->json($data, $status, $headers, $options);
    }
}
