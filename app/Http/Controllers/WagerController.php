<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/7/2020
 * Time: 11:58 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Http\Validators\MustValidateWager;
use App\Repositories\WagerRepository;

use Illuminate\Http\Request;

class WagerController extends Controller{
    use MustValidateWager;

    /**
     * Handle creating wager request
     *
     * @param Request $request
     * @param WagerRepository $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request, WagerRepository $repository){
        $inputs = $request->json()->all();
        $validator = $this->validateCreating($inputs);
        if($validator->fails()){
            return $this->response([
                'error'=> $validator->errors()
            ],400);
        }
        if($result = $repository->store($inputs)){
            return $this->response($result,201);
        }
        else{
            return $this->response([
                'error'=> ERR_CREATING_WAGER_FAILED
            ],500);
        }
    }


    /**
     * Get list wagers
     *
     * @param Request $request
     * @param WagerRepository $repository
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList(Request $request, WagerRepository $repository){
        $page = $request->get('page', 1);
        $limit = $request->get('limit', DEFAULT_PAGE_SIZE);
        $validator = $this->validateGettingList(compact('page','limit'));
        if($validator->fails()){
            return $this->response([
                'error'=> $validator->errors()
            ],400);
        }
        $results = $repository->getList($page, $limit);
        return $this->response($results, 200);
    }
}
