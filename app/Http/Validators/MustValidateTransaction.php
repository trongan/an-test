<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 12:22 PM
 */

namespace App\Http\Validators;

use Illuminate\Support\Facades\Validator;

trait MustValidateTransaction{

    /**buying_price
     * Validate inputs
     *
     * @param $wager
     * @param $inputs
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateBuying($wager, $inputs){
        $rules = [
            'buying_price'=> 'required|numeric|gt:0'
        ];
        $validator = Validator::make($inputs, $rules)
        ->after(function($validator) use ($wager, $inputs){
            if($validator->errors()->isEmpty()){
                if($inputs['buying_price'] > $wager->current_selling_price){
                    $validator->errors()->add('buying_price', 'must be lesser or equal to current_selling_price of the wager_id');
                }
            }
        });
        return $validator;
    }
}