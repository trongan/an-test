<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 11:41 AM
 */

namespace App\Http\Validators;

use Illuminate\Support\Facades\Validator;

trait MustValidateWager
{
    /**
     * Validate inputs
     *
     * @param $inputs
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateCreating($inputs){
        $rules = [
            'total_wager_value'=> 'required|integer|gt:0',
            'odds'=> 'required|integer|gt:0',
            'selling_percentage'=> 'required|integer|between:1,100',
            'selling_price'=> 'required|numeric|gt:0'
        ];
        $validator = Validator::make($inputs, $rules)
        ->after(function($validator) use ($inputs){
            if($validator->errors()->isEmpty()){
                if($inputs['selling_price'] <= $inputs['total_wager_value'] * ($inputs['selling_percentage'] /100)){
                    $validator->errors()->add('selling_price', 'must be greater than total_wager_value * (selling_percentage / 100)');
                }
            }
        });
        return $validator;
    }


    /**
     * @param $inputs
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateGettingList($inputs){
        $rules = [
            'page'=> 'integer|min:1',
            'limit'=> 'integer|min:1'
        ];
        $validator = Validator::make($inputs, $rules);
        return $validator;
    }

}