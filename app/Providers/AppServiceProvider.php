<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){
        //
        ini_set('serialize_precision', -1);
        require_once base_path('app/Helpers/constants.php');
        if($this->app->environment()==='production'){
            error_reporting(E_ALL& ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
        }
        else{
            error_reporting(E_ALL);
        }
    }
}
