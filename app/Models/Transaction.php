<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 12:24 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model{
    /**
     * @var string
     */
    protected $table = 'transactions';


    /**
     *
     */
    const UPDATED_AT = null;

    /**
     *
     */
    const CREATED_AT =  'bought_at';
}