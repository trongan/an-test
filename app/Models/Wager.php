<?php
/**
 * Created by PhpStorm.
 * User: trong an
 * Date: 3/8/2020
 * Time: 11:43 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Wager extends Model {
    /**
     * @var string
     */
    protected $table = 'wagers';


    /**
     *
     */
    const UPDATED_AT = null;

    /**
     *
     */
    const CREATED_AT =  'placed_at';

}